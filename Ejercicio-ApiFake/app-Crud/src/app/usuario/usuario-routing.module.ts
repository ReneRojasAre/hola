import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioComponent } from './usuario.component';
import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';

const routes: Routes = [
  { path: '', redirectTo: 'usuario/inicio', pathMatch: 'full' },
  {
    path: 'usuario', children: [
      { path: 'inicio', component: UsuarioComponent },
      { path: 'crear-usuario', component: CrearUsuarioComponent }
    ]
  }

];

// const routes: Routes = [
//   {path:'', redirectTo:'ejemplo/inicio', pathMatch:'full'},
//   {path:'ejemplo', children: [
//     {path:'inicio', component: EjemploComponent},
//     //{path:'inicio/:id', component: EjemploComponent}, // envia una id por el link, espera un parametro despues del /
//     {path:'ver', component: VerEjemploComponent},
//     {path:'ver/:id', component: VerEjemploComponent},
//     {path:'sobre', component: SobreEjemploComponent}
//   ]},
// ];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
