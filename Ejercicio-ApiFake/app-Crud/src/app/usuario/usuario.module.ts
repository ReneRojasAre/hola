import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuarioRoutingModule } from './usuario-routing.module';
import { UsuarioComponent } from './usuario.component';
import {ReactiveFormsModule} from '@angular/forms';
import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';
import { UsuarioService } from './usuario.service';

@NgModule({
  imports: [
    CommonModule,
    UsuarioRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [UsuarioComponent, CrearUsuarioComponent],
  exports: [UsuarioComponent,CrearUsuarioComponent],
  providers: [UsuarioService]
})
export class UsuarioModule { }
