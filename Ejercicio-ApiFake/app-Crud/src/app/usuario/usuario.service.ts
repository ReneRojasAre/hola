import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Usuario } from './usuario'

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable()
export class UsuarioService {

  private URL_API: string = 'http://localhost:3004/usuarios';

  constructor(
    private httpClient: HttpClient
  ) {

  }

  guardarUsuario(usuario: Usuario): Observable<Usuario> {
    return this.httpClient.post<Usuario>(this.URL_API, usuario);
  }


}
