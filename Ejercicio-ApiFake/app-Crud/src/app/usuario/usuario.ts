export class Usuario {

    public nombre: String;
    public apellidoPaterno: String;
    public apellidoMaterno: String;
    public edad: Number;
    public email: String;
    public id: Number;

    // constructor(nombre: String, apellidoPaterno: String, apellidoMaterno: String, edad: Number, email: String) {
    //     this.nombre = nombre;
    //     this.apellidoPaterno = apellidoPaterno;
    //     this.apellidoMaterno = apellidoMaterno;
    //     this.edad = edad;
    //     this.email = email;

    // }

}
