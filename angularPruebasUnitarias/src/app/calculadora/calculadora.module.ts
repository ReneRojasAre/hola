import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalculadoraRoutingModule } from './calculadora-routing.module';
import { CalculadoraComponent } from './calculadora.component';
import { CalculadoraService } from './calculadora.service';

@NgModule({
  imports: [
    CommonModule,
    CalculadoraRoutingModule
  ],
  declarations: [CalculadoraComponent],
  providers: [CalculadoraService]
})
export class CalculadoraModule { }
