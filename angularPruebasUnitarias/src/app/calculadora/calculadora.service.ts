import { Injectable } from '@angular/core';

@Injectable()
export class CalculadoraService {

  constructor() { }

  public static suma(a, b)
  {
  return (a + b);
  }

  public static resta(a, b)
  {
  return (a - b);
  }
 
}
