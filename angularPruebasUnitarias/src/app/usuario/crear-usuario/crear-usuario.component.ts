import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Usuario } from '../usuario';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {

  @Output()
  public emisorEventoUsuario: EventEmitter<Usuario>;

  public formularioUsuario: FormGroup;

  private usuario: Usuario;

  public nombre: FormControl;
  public apellido: FormControl;
  public edad: FormControl;
  public correo: FormControl;

  constructor(private usuarioService: UsuarioService) {

    this.emisorEventoUsuario = new EventEmitter<Usuario>();

    this.nombre = new FormControl('', [Validators.required]);
    this.apellido = new FormControl('', [Validators.required]);
    this.edad = new FormControl('', [Validators.required]);
    this.correo = new FormControl('', [Validators.required]);


  }

  ngOnInit() {
    this.formularioUsuario = new FormGroup({
      nombre: this.nombre,
      apellido: this.apellido,
      edad: this.edad,
      correo: this.correo,
    });
  }

  crearUsuario() {
    if (this.formularioUsuario.valid) {
      console.log(this.formularioUsuario.value);
      this.usuario = new Usuario();
      this.usuario.nombre = this.nombre.value;
      this.usuario.apellido = this.apellido.value;
      this.usuario.edad = this.edad.value;
      this.usuario.correo = this.correo.value;

      this.emisorEventoUsuario.emit(this.usuario);
      this.formularioUsuario.reset();
    }
  }

}
