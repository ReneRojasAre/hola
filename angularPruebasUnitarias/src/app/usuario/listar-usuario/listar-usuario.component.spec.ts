import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarUsuarioComponent } from './listar-usuario.component';
import { UsuarioService } from '../usuario.service';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { CrearUsuarioComponent } from '../crear-usuario/crear-usuario.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Usuario } from '../usuario';

describe('ListarUsuarioComponent', () => {
  let component: ListarUsuarioComponent;
  let fixture: ComponentFixture<ListarUsuarioComponent>;
  let usuarioService: UsuarioService;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListarUsuarioComponent, CrearUsuarioComponent],
      providers: [UsuarioService],
      imports: [ReactiveFormsModule, HttpClientModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarUsuarioComponent);
    component = fixture.componentInstance;
    usuarioService = TestBed.get(usuarioService);
    debugElement = fixture.debugElement.query(By.css('form'))
    fixture.detectChanges();
  });

  it('sould create', ()=>{
    expect(component).toBeTruthy();
  });



  it('ListarUsuario no es nulo aumentar arreglo de usuario en 1', ()=>{
    let usuario = new Usuario();
    component.agregarUsuarioLista(usuario);
    expect(component.usuarios.length).toBe(1);
  });

  it('ListarUsuario no es nulo aumentar arreglo de usuario en 1', ()=>{
    let usuario = new Usuario();
    component.agregarUsuarioLista(null);
    expect(component.usuarios.length).toBe(0);
  });


});
