import { Component, OnInit } from '@angular/core';
import { Usuario } from '../usuario';
import { UsuarioService } from '../usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listar-usuario',
  templateUrl: './listar-usuario.component.html',
  styleUrls: ['./listar-usuario.component.css']
})
export class ListarUsuarioComponent implements OnInit {

  public usuarios: Usuario[];


  constructor() {
    this.usuarios = [];
  }

  ngOnInit() {

  }



  agregarUsuarioLista(usuario) {
    console.log(usuario);

    if(null != usuario ){
      this.usuarios.push(usuario);
    }

  }


}
