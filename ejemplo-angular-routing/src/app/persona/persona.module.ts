import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonaRoutingModule } from './persona-routing.module';
import { PersonaComponent } from './persona.component';

@NgModule({
  imports: [
    CommonModule,
    PersonaRoutingModule
  ],
  declarations: [PersonaComponent]
})
export class PersonaModule { }
