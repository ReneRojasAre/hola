import { Component, OnInit, ViewChild } from '@angular/core';
import { Usuario } from '../usuario';
import { UsuarioService } from '../usuario.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css'],
  providers: [UsuarioService]
})
export class CrearUsuarioComponent implements OnInit {

  // @ViewChild('formUsuario') formUsuario: any;
  public usuario: Usuario;
  public sexoArreglo = ['Masculino', 'Femenino'];
  public formUsuario: FormGroup;


  constructor(
    private usuarioService: UsuarioService
  ) {
    this.usuario = new Usuario();
  }

  ngOnInit() {
  }

  crearUsuario() {

    if (this.formUsuario.valid) {
      console.log(this.usuario);
      this.usuarioService.guardarUsuario(this.usuario).subscribe(res => {
        console.log(res)
        this.formUsuario.reset();
      })
    }

  }

}
