import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'mayorEdad'
})
export class MayorEdadPipe implements PipeTransform {

  private Mayor_de_Edad: Number = 18;

  transform(value: any): any {
    console.log('pasa por pipe' + value);
    if (value >= this.Mayor_de_Edad) {
      return 'Es mayor de Edad';
    } else {
      return 'Es menor de Edad';
    }
  }

}
