import { Component, OnInit } from '@angular/core';
import { Usuario } from './usuario';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MayorEdadPipe } from './mayor-edad.pipe';
import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  public usuario: Usuario;
  public formUsuario: FormGroup;
  public sexos: String[];
  public listaUsuarios: Usuario[];

  constructor() {
    this.listaUsuarios =[];
  }

  ngOnInit() {   //

    this.formUsuario = new FormGroup({
      nombre: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(10)]),
      edad: new FormControl(0, Validators.required),
      sexo: new FormControl('', Validators.required)

    });

    this.sexos = ['Masculino', 'Femenino'];

  }


  submitFormulario(lista) {

    // // this.usuario.nombre = this.formUsuario.controls.nombre.value;
    // // this.usuario.edad = this.formUsuario.controls.edad.value;
    // // this.usuario.sexo = this.formUsuario.controls.sexo.value;

    // // this.usuario = this.formUsuario.value;
    // // this.usuario;
    // this.usuario = this.formUsuario.value;
    // // lista = this.usuario;

    // // if(lista.length > 0){
    // //   return lista;
    // // }

    // console.log(this.usuario);

    if(this.formUsuario.valid){
      this.usuario = new Usuario();
      this.usuario.nombre= this.formUsuario.controls.nombre.value;
      this.usuario.edad= this.formUsuario.controls.edad.value;
      this.usuario.sexo= this.formUsuario.controls.sexo.value;
      this.listaUsuarios.push(this.usuario);
      this.formUsuario.reset();
    }
    

  }

  cambiarNombre() {

  }

}
