import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Usuario } from './usuario'


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}



@Injectable()
export class UsuarioService {

  private URL_API: string = 'http://localhost:3004/usuarios';

  constructor(
    private httpClient: HttpClient
  ) { }

  // buscarUsuarios(): Observable<Usuario[]> {
  //   return this.httpClient.get<Usuario[]>(this.URL_API);
  // }

  guardarUsuario(usuario: Usuario): Observable<Usuario> {
    return this.httpClient.post<Usuario>(this.URL_API, usuario);
  }

//   editarUsuario(usuario: Usuario): Observable<Usuario> {
//     return this.httpClient.put<Usuario>(this.URL_API + '/' + usuario.id, usuario, httpOptions);
//   }

//   eliminarUsuario(usuario: Usuario): Observable<Usuario> {
//     return this.httpClient.delete<Usuario>(this.URL_API + '/' + usuario.id);
//   }

}
