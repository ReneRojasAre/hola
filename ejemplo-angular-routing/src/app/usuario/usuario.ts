export class Usuario {
    public nombre: String;
    public edad: Number;
    public sexo: String;
    public id: Number;
    // public MayorEdad: String;

    // constructor(nombre: String, edad: Number, sexo:String, MayorEdad: String) {
    //     this.nombre = nombre;
    //     this.edad=edad;
    //     this.sexo=sexo;
    //     this.MayorEdad=MayorEdad;
    //   }

    cargarUsuario(nombre: String, edad: Number, sexo: String) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
    }

}
