import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibrosRoutingModule } from './libros-routing.module';

@NgModule({
  imports: [
    CommonModule,
    LibrosRoutingModule
  ],
  declarations: []
})
export class LibrosModule { }
