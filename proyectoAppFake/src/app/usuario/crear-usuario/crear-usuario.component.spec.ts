import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearUsuarioComponent } from './crear-usuario.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UsuarioService } from '../usuario.service';
import { HttpClientModule } from '@angular/common/http';
import { DebugElement } from '@angular/core';
import { By } from 'selenium-webdriver';
import { Usuario } from '../usuario';

describe('CrearUsuarioComponent', () => {
  let component: CrearUsuarioComponent;
  let fixture: ComponentFixture<CrearUsuarioComponent>;
  let usuarioService: UsuarioService;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CrearUsuarioComponent],
      imports: [ReactiveFormsModule, HttpClientModule],
      providers: [UsuarioService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearUsuarioComponent);
    component = fixture.componentInstance;
    usuarioService = TestBed.get(UsuarioService);
    // debugElement = fixture.debugElement.query(By.css('a'));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('crearUsuario| datos nulos | retorna null', () => {
    spyOn(usuarioService,'crearUsuario').and.returnValue(null);
    expect(component.crearUsuario()).toBeNull;
  });

  // it('crearUsuario| datos validos de usuario| retorna usuario',()=>{
  //   component.nombre.setValue('Juan');
  //   component.apellidoPaterno.setValue('Perez');
  //   component.apellidoMaterno.setValue('Perez');
  //   component.edad.setValue(24);
  //   component.email.setValue('Juanperez@.cl');
  //   component.saldo.setValue(4000);
  //   spyOn(usuarioService, 'crearUsuario').and.returnValue(component.formularioUsuario.value);
  //   let usuarios= null;
  //   component.crearUsuario().subscribe((usuario)=>{
  //     usuarios = usuario;
  //   });
  //   expect(component.formularioUsuario.valid).toBeFalsy;
  // });
  // it('CrearUsuario con valores ingresados crea objeto usuario con datos de usuario y lo emite', () => {
  //   component.nombre.setValue('Juan');
  //   component.apellidoPaterno.setValue('Perez');
  //   component.apellidoMaterno.setValue('Perez');
  //   component.edad.setValue(24);
  //   component.email.setValue('Juanperez@.cl');
  //   component.saldo.setValue(4000);
  //   expect(component.formularioUsuario.valid).toBeTruthy();
  //   let usuario = null;
  //   component.emisorEventoUsuario.subscribe((usuarioEmitido) => {
  //     usuario = usuarioEmitido;
  //   });

  //   component.crearUsuario();
  //   expect(usuario.nombre).toBe('Juan');
  //   expect(usuario.apellidoPaterno).toBe('Perez');
  //   expect(usuario.apellidoMaterno).toBe('Perez');
  //   expect(usuario.edad).toBe(24);
  //   expect(usuario.email).toBe('Juanperez@.cl');
  //   expect(usuario.saldo).toBe(4000);
  // });

//Lista de todos los usuarios
  // it ('should get users', async(() => {
  //   const service: UsuarioService = TestBed.(usuarioService);
  //   service.listarTodosLosUsuarios().subscribe(
  //     (response) => expect(response).not.toBeNull(),
  //     (error) => fail(error)
  //   );
  // }));
});
