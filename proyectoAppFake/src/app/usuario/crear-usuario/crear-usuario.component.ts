import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Usuario } from '../usuario';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {

  @Output()
  public emisorEventoUsuario: EventEmitter<Usuario>;

  public formularioUsuario: FormGroup;

  private usuario: Usuario;

  public nombre: FormControl;
  public apellidoPaterno: FormControl;
  public apellidoMaterno: FormControl;
  public edad: FormControl;
  public email: FormControl;
  public saldo: FormControl;

  constructor(private usuarioService: UsuarioService) {

    this.emisorEventoUsuario = new EventEmitter<Usuario>();

    this.nombre = new FormControl('', [Validators.required, Validators.maxLength(15), Validators.pattern('[a-zA-Z]*')]);
    this.apellidoPaterno = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*')]);
    this.apellidoMaterno = new FormControl('', Validators.pattern('[a-zA-Z]*'));
    this.edad = new FormControl('', [Validators.required, Validators.pattern('[0-9]*')]);
    this.email = new FormControl('', [Validators.required, Validators.email]);
    this.saldo = new FormControl('', Validators.required);

  }

  ngOnInit() {
    this.formularioUsuario = new FormGroup({
      nombre: this.nombre,
      apellidoPaterno: this.apellidoPaterno,
      apellidoMaterno: this.apellidoMaterno,
      edad: this.edad,
      email: this.email,
      saldo: this.saldo
    });
  }

  crearUsuario() {
    if (this.formularioUsuario.valid) {
      console.log(this.formularioUsuario.value);
      this.usuario = new Usuario();
      this.usuario.nombre = this.nombre.value;
      this.usuario.apellidoPaterno = this.apellidoPaterno.value;
      this.usuario.apellidoMaterno = this.apellidoMaterno.value;
      this.usuario.edad = this.edad.value;
      this.usuario.email = this.email.value;
      this.usuario.saldo= this.saldo.value;

      this.usuarioService.crearUsuario(this.usuario).subscribe((usuario) => {
        console.log(usuario);
        this.emisorEventoUsuario.emit(usuario);
        this.formularioUsuario.reset();
      });
    }
  }

}
