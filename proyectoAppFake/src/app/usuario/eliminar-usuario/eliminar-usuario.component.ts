import { Component, OnInit } from '@angular/core';
import { Usuario } from '../usuario';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { UsuarioService } from '../usuario.service';

@Component({
  selector: 'app-eliminar-usuario',
  templateUrl: './eliminar-usuario.component.html',
  styleUrls: ['./eliminar-usuario.component.css']
})
export class EliminarUsuarioComponent implements OnInit {
  private id: Number;
  private usuario: Usuario;
  public formularioEditarUsuario: FormGroup;

  constructor(private usuarioService: UsuarioService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.params.forEach((params: Params) => {
      console.log('recibo id:' + params['id']);
      this.id = params['id'];
    });

    this.usuarioService.buscarUsuarioPorId(this.id).subscribe(
      (usuario) => {
        this.usuario = usuario;
      });
  }

  eliminarUsuarioBD() {

    this.usuarioService.eliminarUsuario(this.usuario).subscribe((usuario) => {
      console.log(usuario);
    });
  }

}
