import { Component, OnInit } from '@angular/core';
import { Usuario } from '../usuario';
import { UsuarioService } from '../usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listar-usuario',
  templateUrl: './listar-usuario.component.html',
  styleUrls: ['./listar-usuario.component.css']
})
export class ListarUsuarioComponent implements OnInit {
  public usuarios:Usuario[];
  

  constructor( private usuarioService: UsuarioService, private router: Router) { }

  ngOnInit() {
    this.usuarioService.listarTodosLosUsuarios().subscribe((usuarios)=>{
      this.usuarios= usuarios;
    })
  }

  redireccionarAEditar(id){
    console.log(id);
    this.router.navigate(['/usuario/modificar',id])
  }

  redireccionarAEliminar(id){
    console.log(id);
    this.router.navigate(['/usuario/eliminar',id])
  }

  agregarUsuarioLista(usuario){
    console.log(usuario);
    this.usuarios.push(usuario);
  }

}
