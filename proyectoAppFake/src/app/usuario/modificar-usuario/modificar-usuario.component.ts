import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UsuarioService } from '../usuario.service';
import { Usuario } from '../usuario';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-modificar-usuario',
  templateUrl: './modificar-usuario.component.html',
  styleUrls: ['./modificar-usuario.component.css']
})
export class ModificarUsuarioComponent implements OnInit {
  private id: Number;
  private usuario: Usuario;
  public formularioEditarUsuario: FormGroup;

  public nombre: FormControl;
  public apellidoPaterno: FormControl;
  public apellidoMaterno: FormControl;
  public edad: FormControl;
  public email: FormControl;


  constructor(private usuarioService: UsuarioService, private activatedRoute: ActivatedRoute) {

    this.nombre = new FormControl('', [Validators.required, Validators.maxLength(15), Validators.pattern('[a-zA-Z]*')]);
    this.apellidoPaterno = new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*')]);
    this.apellidoMaterno = new FormControl('', Validators.pattern('[a-zA-Z]*'));
    this.edad = new FormControl('', [Validators.required, Validators.pattern('[0-9]*')]);
    this.email = new FormControl('', [Validators.required, Validators.email]);

  }

  ngOnInit() {
    this.activatedRoute.params.forEach((params: Params) => {
      console.log('recibo id:' + params['id']);
      this.id = params['id'];
    });

    this.usuarioService.buscarUsuarioPorId(this.id).subscribe(
      (usuario) => {
        this.usuario = usuario;
        this.nombre.setValue(this.usuario.nombre);
        this.apellidoPaterno.setValue(this.usuario.apellidoPaterno);
        this.apellidoMaterno.setValue(this.usuario.apellidoMaterno);
        this.edad.setValue(this.usuario.edad);
        this.email.setValue(this.usuario.email);
      });
    this.formularioEditarUsuario = new FormGroup({
      nombre: this.nombre,
      apellidoPaterno: this.apellidoPaterno,
      apellidoMaterno: this.apellidoMaterno,
      edad: this.edad,
      email: this.email
    });

  }

  modificarUsuario() {
    if (this.formularioEditarUsuario.valid) {
      this.usuario.nombre = this.nombre.value;
      this.usuario.apellidoPaterno = this.apellidoPaterno.value;
      this.usuario.apellidoMaterno = this.apellidoMaterno.value;
      this.usuario.edad = this.edad.value;
      this.usuario.email = this.email.value;

      this.usuarioService.editarUsuario(this.usuario).subscribe((usuario) => {
        console.log(usuario);
        this.formularioEditarUsuario.reset();
      });
      
    }

  }

}



