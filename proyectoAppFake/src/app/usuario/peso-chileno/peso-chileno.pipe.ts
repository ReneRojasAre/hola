import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pesoChileno'
})
export class PesoChilenoPipe implements PipeTransform {

  transform(value: any, args?: number): any {
    var flotante = parseFloat(value);
    return "CLP/. " + flotante.toFixed(args);
  }

}
