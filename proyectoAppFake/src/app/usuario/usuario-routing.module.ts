import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioComponent } from './usuario.component';
import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';
import { ListarUsuarioComponent } from './listar-usuario/listar-usuario.component';
import { ModificarUsuarioComponent } from './modificar-usuario/modificar-usuario.component';
import { EliminarUsuarioComponent } from './eliminar-usuario/eliminar-usuario.component';

const routes: Routes = [
  { path: '', redirectTo: 'usuario/crear', pathMatch: 'full' },
  {
    path: 'usuario', children: [
      { path: 'crear', component: CrearUsuarioComponent },
      { path: 'listar', component: ListarUsuarioComponent },
      { path: 'modificar', component: ModificarUsuarioComponent },
      { path: 'modificar/:id', component: ModificarUsuarioComponent },
      { path: 'eliminar', component: EliminarUsuarioComponent },
      { path: 'eliminar/:id', component: EliminarUsuarioComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
