import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuarioRoutingModule } from './usuario-routing.module';
import { UsuarioComponent } from './usuario.component';
import { CrearUsuarioComponent } from './crear-usuario/crear-usuario.component';
import { ListarUsuarioComponent } from './listar-usuario/listar-usuario.component';
import { ModificarUsuarioComponent } from './modificar-usuario/modificar-usuario.component';
import { EliminarUsuarioComponent } from './eliminar-usuario/eliminar-usuario.component';
import { UsuarioService } from './usuario.service';
import {ReactiveFormsModule} from '@angular/forms'
import { HttpClient } from 'selenium-webdriver/http';
import { HttpClientModule } from '@angular/common/http';
import { UsuarioPipePipe } from './usuario-pipe/usuario-pipe.pipe';
import { PesoChilenoPipe } from './peso-chileno/peso-chileno.pipe';

@NgModule({
  imports: [
    CommonModule,
    UsuarioRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [UsuarioComponent, CrearUsuarioComponent, ListarUsuarioComponent, ModificarUsuarioComponent, EliminarUsuarioComponent, UsuarioPipePipe, PesoChilenoPipe],
  providers: [UsuarioService],
  exports: [UsuarioComponent]
})
export class UsuarioModule { }
