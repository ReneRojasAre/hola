import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {Usuario} from './usuario'
@Injectable()
export class UsuarioService {

  public URL_API: string ="http://localhost:3004/usuarios"

  //por inyeccion de dependencia hacemos el httpclient
  constructor(private httpClient: HttpClient) { }

  crearUsuario(usuario:Usuario): Observable<Usuario>{
    return this.httpClient.post<Usuario>(this.URL_API,usuario);
  }

  listarTodosLosUsuarios(): Observable<Usuario[]>{
    return this.httpClient.get<Usuario[]>(this.URL_API);
  }

  editarUsuario(usuario:Usuario):Observable<Usuario>{
    return this.httpClient.put<Usuario>(this.URL_API + '/' + usuario.id, usuario);
  }

  buscarUsuarioPorId(id:Number):Observable<Usuario>{
    return this.httpClient.get<Usuario>(this.URL_API+'/'+ id);
  }

  eliminarUsuario(usuario:Usuario):Observable<Usuario>{
    return this.httpClient.delete<Usuario>(this.URL_API+'/'+usuario.id);
  }

}
