export class Usuario {
    public id: Number;
    public nombre: String;
    public apellidoPaterno: String;
    public apellidoMaterno: String;
    public edad: Number;
    public email: String;
    public hobbies: String[];
    public saldo: Number;
}
