import { Injectable } from '@angular/core';
import { Persona } from '../persona/persona';

const KEY_USUARIOS = 'personas';

@Injectable()
export class AutenticacionService {

  constructor() { }

  
  static guardarEnSesion(nombre : string, objeto : string) {
    localStorage.setItem(nombre, objeto);
  }

  static buscarEnSesion(nombre:string) : string {
    return localStorage.getItem(nombre);
  }

  static limpiarSesion() {
    localStorage.clear();
  }

  static guardarPersonaEnSesion(persona : Persona) {
    this.guardarEnSesion(KEY_USUARIOS, JSON.stringify(persona));
  }

  static buscarPersonaEnSesion() : Persona {
    if (null == this.buscarEnSesion(KEY_USUARIOS)){
      return null;
    }
    let usuario = new Persona();
    usuario = JSON.parse(this.buscarEnSesion(KEY_USUARIOS));
      return usuario;
  }

    //CERRAR SESION

    static cerrarSesion() {
      localStorage.clear();
    }

}
