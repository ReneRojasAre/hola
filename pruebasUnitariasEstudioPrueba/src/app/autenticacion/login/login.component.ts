import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PersonaService } from '../../persona/persona.service';
import { Router } from '@angular/router';
import { Persona } from '../../persona/persona';
import { AutenticacionService } from '../autenticacion.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public formLogin: FormGroup;
  public usuario: Persona;
  public correo: FormControl;
  public contrasena: FormControl

  constructor(private personaService: PersonaService, private router: Router) {
    this.correo = new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$')
    ]);
    this.contrasena = new FormControl('', Validators.required);
  }

  ngOnInit() {
    this.formLogin = new FormGroup({
      correo: this.correo,
      contrasena: this.contrasena
    });
  }

  iniciarSesion() {
    if (this.formLogin.valid) {
      //console.log(this.email.value);
      let usuario = new Persona();
      usuario.contrasena = this.contrasena.value;
      usuario.correo = this.correo.value;
      this.personaService.loginUsuario(usuario).subscribe(
        (val) => {
          usuario = val;
          console.log(usuario);
          if (usuario != null) {
            usuario.contrasena = '';
            AutenticacionService.guardarPersonaEnSesion(usuario);
            this.router.navigate(['/persona']);
          }
        },
        (error) => {
          console.log('error');
          console.log(error);
        }
      )
    }
  }

}
