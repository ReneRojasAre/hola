import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonaComponent } from './persona.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PersonaService } from './persona.service';
import { AutenticacionService } from '../autenticacion/autenticacion.service';
import { HttpClientModule } from '@angular/common/http';

describe('PersonaComponent', () => {
  let component: PersonaComponent;
  let fixture: ComponentFixture<PersonaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PersonaComponent],
      imports: [ReactiveFormsModule, HttpClientModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
