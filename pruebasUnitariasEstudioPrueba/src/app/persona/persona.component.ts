import { Component, OnInit } from '@angular/core';
import { AutenticacionService } from '../autenticacion/autenticacion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  cerrarSesion() {
    console.log("Sesion Cerrada");
    AutenticacionService.cerrarSesion();
    this.router.navigate(['']);
  }
}
