import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonaRoutingModule } from './persona-routing.module';
import { PersonaComponent } from './persona.component';
import { PersonaService } from './persona.service';
import { LoginComponent } from '../autenticacion/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CrearPersonaComponent } from './crear-persona/crear-persona.component';
import { ListarPersonaComponent } from './listar-persona/listar-persona.component';

@NgModule({
  imports: [
    CommonModule,
    PersonaRoutingModule,
    
  ],
  declarations: [PersonaComponent, CrearPersonaComponent, ListarPersonaComponent],
  providers: [PersonaService]
})
export class PersonaModule { }
