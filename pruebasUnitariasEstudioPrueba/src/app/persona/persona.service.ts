import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Persona } from './persona';

@Injectable()
export class PersonaService {

  private URL_API = 'http://localhost:3004/personas';

  constructor(private httpClient : HttpClient) { }

  buscarUsuarios() : Observable<Persona[]> {
    return this.httpClient.get<Persona[]>(this.URL_API);
  }

  buscarUsuario(id : Number) : Observable<Persona> {
    return this.httpClient.get<Persona>(this.URL_API + '/' + id);
  }

  /*guardarUsuario(usuario : Usuario) : Observable<Usuario> {
    return this.httpClient.post<Usuario>(this.URL_API, usuario);
  }

  editarUsuario(usuario : Usuario) : Observable<Usuario[]> {
    return this.httpClient.put<Usuario[]>(this.URL_API + '/' + usuario.id, usuario);
  }

  eliminarUsuario(usuario : Usuario) : Observable<Usuario[]> {
    return this.httpClient.delete<Usuario[]>(this.URL_API + '/' + usuario.id);
  }*/

  loginUsuario(usuario : Persona) {
    let httpParams = new HttpParams();
    httpParams = httpParams.set('contrasena', usuario.contrasena.toString());
    httpParams = httpParams.set('correo', usuario.correo.toString());
    console.log(httpParams.toString());
    return this.httpClient.get<Persona>(this.URL_API, {params : httpParams});
  }

}
