export class Persona {
    public nombre: String;
    public apellido: String;
    public correo: String;
    public edad: String;
    public sexo: String;
    public contrasena: String;
    public id: Number;
}
