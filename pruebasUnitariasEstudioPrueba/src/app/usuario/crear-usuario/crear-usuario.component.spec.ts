import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearUsuarioComponent } from './crear-usuario.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('CrearUsuarioComponent', () => {
  let component: CrearUsuarioComponent;
  let fixture: ComponentFixture<CrearUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CrearUsuarioComponent],
      imports: [ReactiveFormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('CrearUsuario con valores ingresados crea objeto usuario con datos de usuario y lo emite', () => {
    component.nombre.setValue('Juan');
    component.apellido.setValue('Perez');
    component.edad.setValue(24);
    component.correo.setValue('Juanperez@.cl');
    expect(component.formularioUsuario.valid).toBeTruthy();
    let usuario = null;
    component.emisorEventoUsuario.subscribe((usuarioEmitido) => {
      usuario = usuarioEmitido;
    });

    component.crearUsuario();
    expect(usuario.nombre).toBe('Juan');
    expect(usuario.apellido).toBe('Perez');
    expect(usuario.edad).toBe(24);
    expect(usuario.correo).toBe('Juanperez@.cl');
  });



});
