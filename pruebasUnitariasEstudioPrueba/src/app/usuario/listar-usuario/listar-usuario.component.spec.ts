import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarUsuarioComponent } from './listar-usuario.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CrearUsuarioComponent } from '../crear-usuario/crear-usuario.component';
import { DebugElement } from '@angular/core';
import { Usuario } from '../usuario';

describe('ListarUsuarioComponent', () => {
  let component: ListarUsuarioComponent;
  let fixture: ComponentFixture<ListarUsuarioComponent>;
  let debugElement : DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarUsuarioComponent , CrearUsuarioComponent],
      imports: [ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('ListarUsuario usuario no es null arreglo de usuarios aumenta en 1', () => {
    let usuario = new Usuario();
    component.agregarUsuarioLista(usuario);
    expect(component.usuarios.length).toBe(1)
  });

  it('ListarUsuario usuario es null arreglo de usuarios aumenta en 1', () => {
    component.agregarUsuarioLista(null);
    expect(component.usuarios.length).toBe(0)
  });

});
