export class Usuario {
    public nombre: String;
    public apellido: String;
    public edad: Number;
    public correo: String;
    public id: Number;
}
